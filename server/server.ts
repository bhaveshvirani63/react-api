import express, {Application, Request, Response} from 'express';
import dotEnv from 'dotenv';
import cors from 'cors';
import {DBUtil} from "./util/DBUtil";
import userRouter from "./router/userRouter";
import profileRouter from "./router/profileRouter";
import postRouter from "./router/postRouter";

const app:Application = express();

dotEnv.config({
    path : './.env'
})

// express to read form data
app.use(express.json());

// configure cors
app.use(cors());

// connect to DB
const dbName : string | undefined = process.env.DATABASE_NAME;
const dbUrl : string | undefined = process.env.MONGO_DB_URL;

// connect to Mongo DB
if(dbName && dbUrl){
    // DBUtil.connectToDB(dbUrl, dbName).then((response) => {
    //     console.log(response);
    // }).catch((error) => {
    //     console.error(error);
    // });
}

const port:number | undefined = Number(process.env.PORT) || 9999;

app.get('/', (request:Request, response : Response) => {
    response.status(200).json({
        msg : 'Welcome to React Social App from Express!'
    });
});

// router configuration
app.use('/api/users', userRouter);
app.use('/api/profiles', profileRouter);
app.use('/api/posts', postRouter);

if(port){
    app.listen(port, () => {
        console.log(`Express Server is started at123 : ${port}`);
    })
}


