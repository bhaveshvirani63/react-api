import {Request, Response, Router} from 'express';
import {body, validationResult} from "express-validator";
import tokenVerifier from "../middleware/TokenVerifier";
import constants from '../constants.json';
import mongoose from 'mongoose';
import {IUser} from "../models/IUser";
import UserTable from "../database/userSchema";
import {IComment, ILike, IPost} from "../models/IPost";
import PostTable from "../database/PostTable";
import { DBUtil } from '../util/DBUtil';
// const multer = require('multer');
import multer, { Multer } from 'multer';
const postRouter: Router = Router();
import path from 'path'; // Import the 'path' module
import fs from 'fs'; // Import the 'fs' module for file system operations

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'uploads/'); // Specify the directory where files will be stored
    },
    filename: (req, file, cb) => {
      const extname = path.extname(file.originalname);
      cb(null, `${Date.now()}${extname}`); // Rename the file to avoid overwriting
    },
  });
  const uploadDirectory = 'uploads';
  const upload = multer({ storage });
  if (!fs.existsSync(uploadDirectory)) {
    fs.mkdirSync(uploadDirectory);
  }

/*
    @sno : 1
    @usage : Create a Post
    @url : http://127.0.0.1:9999/api/posts/
    @fields : image , text
    @method : POST
    @access : PRIVATE
 */
    // postRouter.post('/upload', upload.single('image'), (req: Request, res: Response) => {

postRouter.post('/', [
    // body('image').not().isEmpty().withMessage('Image is Required'),
    // body('text').not().isEmpty().withMessage('Text is Required'),
],upload.single('imageFile'), tokenVerifier, async (request: Request, response: Response) => {

    // validate the form from client
    console.log('f=======================')
    console.log('f=======================', request)
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({errors: errors.array()});
    }

    try {
        if (request.file) {
             console.log('file uploaded success fully')
          }
        let {text, image} = request.body;
        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];
        
        const connection = await DBUtil.connectToDB()

        const userQuery = 'SELECT  user_id,name,email,isAdmin,avatar  FROM users WHERE user_id = ?';
        console.log('User ID:', requestedUser.id);
        const resultdata = await queryDatabase(connection, userQuery, [requestedUser.id]);
        if (resultdata && Array.isArray(resultdata) && resultdata.length > 0) {
            // The results object exists, is an array, and contains at least one record
            const userRecord = resultdata[0]; // Assuming it's the first record
            const userId = userRecord.user_id;
            console.log('User ID:', userId);

            let postObj: IPost = {
                user_id: requestedUser.id,
                text: text,
                image: image,
                name: userRecord.name,
                avatar: userRecord.avatar,
                likes: [] as ILike[],
                comments: [] as IComment[],
                 
            };

            const query = `
            INSERT INTO posts (user_id ,text, image, name, avatar)
            VALUES (?,?, ?, ?, ?)
          `;
            const values = [postObj.user_id, postObj.text, postObj.image, postObj.name,
                postObj.avatar ];
    
            const insertedEducation: any = await queryDatabase(connection, query, values)
            return response.status(200).json({
                msg: 'Post is created!',
                post: postObj
            });
        } else {
            return response.status(404).json({
                msg: 'User not found!'
            });
        }

        
        

        
       

    } catch (error) {
        console.log(error)
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
    @sno : 2
    @usage : Get All the posts
    @url : http://127.0.0.1:9999/api/posts/
    @fields : no-fields
    @method : GET
    @access : PRIVATE
 */
postRouter.get('/', tokenVerifier, async (request: Request, response: Response) => {
    try {

        const connection = await DBUtil.connectToDB()
        const userQuery = `SELECT  *  FROM posts `;
         const postData:any = await queryDatabase(connection, userQuery, []);
         var allPosts: IPost[] = []
        if (postData && Array.isArray(postData) && postData.length > 0) {

            for (let index = 0; index < postData.length; index++) {
                const element = postData[index];
                const postLikeQuery = `SELECT  *  FROM post_like where post_id = ${element.post_id }`
                const postLikeData: any = await queryDatabase(connection, postLikeQuery, []);
                
                const post: any =  await creatPostObject(element.post_id )
                
                allPosts.push(post)
                
            }
           
                

            
            
            return response.status(200).json({
                posts: allPosts
            })
        }

       

        
         
    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
    @sno : 3
    @usage : Get a post with Post Id
    @url : http://127.0.0.1:9999/api/posts/:postId
    @fields : no-fields
    @method : GET
    @access : PRIVATE
 */
postRouter.get('/:postId', tokenVerifier, async (request: Request, response: Response) => {
    try {
        const {postId} = request.params;
 
        const connection = await DBUtil.connectToDB()
        const userQuery = `SELECT  *  FROM posts where post_id = ${postId } `;
         const resultdata: any = await queryDatabase(connection, userQuery, []);
        if (resultdata && Array.isArray(resultdata) && resultdata.length > 0) {
            return response.status(200).json({
                post: resultdata
            });
            
        } else {
            return response.status(404).json({
                errors: [
                    {
                        msg: 'No Posts Found!'
                    }
                ]
            });
        }

       
    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
    @sno : 4
    @usage : Delete a post with Post Id
    @url : http://127.0.0.1:9999/api/posts/:postId
    @fields : no-fields
    @method : DELETE
    @access : PRIVATE
 */
postRouter.delete('/:postId', tokenVerifier, async (request: Request, response: Response) => {
    try {
        const {postId} = request.params;
        const mongoPostId = new mongoose.Types.ObjectId(postId);

        const connection = await DBUtil.connectToDB()
        const userQuery = `SELECT  *  FROM posts where post_id = ${postId } `;
         const resultdata: any = await queryDatabase(connection, userQuery, []);
        if (resultdata && Array.isArray(resultdata) && resultdata.length == 0) {
            return response.status(404).json({
                errors: [
                    {
                        msg: 'No Posts Found!'
                    }
                ]
            });
        }

        const deleteQuery = `DELETE posts where post_id = ${postId } `;
        const deletedData: any = await queryDatabase(connection, deleteQuery, []);
         
       
        return response.status(200).json({
            msg: 'Post is Removed!',
            post: resultdata
        });
    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
    @sno : 5
    @usage : Like A Post with PostId
    @url : http://127.0.0.1:9999/api/posts/like/:postId
    @fields : no-fields
    @method : POST
    @access : PRIVATE
 */
postRouter.post('/like/:postId', tokenVerifier, async (request: Request, response: Response) => {
    try {
        let {postId} = request.params;
        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];

        const connection = await DBUtil.connectToDB()
        const postSelectQuery = `SELECT  *  FROM posts where post_id = ${postId } `;
        const postData: any = await queryDatabase(connection, postSelectQuery, []);
        if (postData && Array.isArray(postData) && postData.length == 0) {
            return response.status(400).json({
                errors: [
                    {
                        msg: 'No Posts Found for the Post ID'
                    }
                ]
            });
        } else {
            const postLikeQuery = `SELECT  *  FROM post_like where post_id = ${postId } AND user_id = ${requestedUser.id} `;
            console.log('postLikeQuery: ', postLikeQuery)
            const postLikeData: any = await queryDatabase(connection, postLikeQuery, []);
            if (postLikeData && Array.isArray(postLikeData) && postLikeData.length > 0) {
                return response.status(400).json({
                    errors: [
                        {
                            msg: 'Post has already been liked'
                        }
                    ]
                });
            } else {
                const query = `
                INSERT INTO post_like (user_id ,post_id)
                VALUES (?,?)
              `;
                const values = [requestedUser.id, postId ];
        
                const insertedEducation: any = await queryDatabase(connection, query, values)
                
                response.status(200).json({
                    post: await creatPostObject(postId)
                });
            }

        }

        
        
    } catch (error) {
        response.status(500).json({
            errors: [
                {
                    msg: error
                }
            ]
        });
    }
});

/*
    @sno : 6
    @usage : Un-Like A Post with PostId
    @url : http://127.0.0.1:9999/api/posts/unlike/:postId
    @fields : no-fields
    @method : PUT
    @access : PRIVATE
 */
postRouter.put('/unlike/:postId', tokenVerifier, async (request: Request, response: Response) => {
    try {
        let {postId} = request.params;
         let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];

        const connection = await DBUtil.connectToDB()
        const userQuery = `SELECT  *  FROM posts where post_id = ${postId } `;
        const postData: any = await queryDatabase(connection, userQuery, []);
        if (postData && Array.isArray(postData) && postData.length == 0) {
            return response.status(400).json({
                errors: [
                    {
                        msg: 'No Posts Found for the Post ID'
                    }
                ]
            });
        } else {
            const postLikeQuery = `SELECT  *  FROM post_like where post_id = ${postId } AND user_id = ${requestedUser.id} `;
            console.log('postLikeQuery: ', postLikeQuery)
            const postLikeData: any = await queryDatabase(connection, postLikeQuery, []);
            if (postLikeData && Array.isArray(postLikeData) && postLikeData.length == 0) {
                return response.status(400).json({
                    errors: [
                        {
                            msg: 'Post has not been liked'
                        }
                    ]
                });
            } else {
                const deleteQuery = `DELETE FROM post_like where post_id = ${postId } AND user_id = ${requestedUser.id}  `;
                const deletedData: any = await queryDatabase(connection, deleteQuery, []);
                 
 
                response.status(200).json({
                    post: await creatPostObject(postId)
                });
            }

        }

         
    } catch (error) {
        console.log("Error:", error)
        response.status(500).json({
            errors: [
                {
                    msg: error
                }
            ]
        });
    }
});

/*
    @sno : 7
    @usage : Create Comment to a post
    @url : http://127.0.0.1:9999/api/posts/comment/:postId
    @fields : text
    @method : POST
    @access : PRIVATE
 */
postRouter.post('/comment/:postId', [
    body('text').not().isEmpty().withMessage('Text is Required'),
], tokenVerifier, async (request: Request, response: Response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({errors: errors.array()});
    }
    try {
        let {text} = request.body;
        let {postId} = request.params;
        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];

        const connection = await DBUtil.connectToDB()
        const postSelectQuery = `SELECT  *  FROM posts where post_id = ${postId } `;
        const postData: any = await queryDatabase(connection, postSelectQuery, []);
        if (postData && Array.isArray(postData) && postData.length == 0) {
            return response.status(400).json({
                errors: [
                    {
                        msg: 'No Posts Found for the Post ID'
                    }
                ]
            });
        } else {

            const userSelectQuery = `SELECT  *  FROM users where user_id = ${requestedUser.id } `;
            const userData: any = await queryDatabase(connection, userSelectQuery, []);
            if (userData && Array.isArray(userData) && userData.length >  0) {
                const userRecord = userData[0]
                 
                const commentInsertQuery = `
                INSERT INTO post_comment (user_id ,post_id, text , name, avatar)
                VALUES (?,?,?,?,?)
              `;
                const commentValues = [requestedUser.id, postId, text, userRecord.name, userRecord.avatar ];
        
                const insertedComment: any = await queryDatabase(connection, commentInsertQuery, commentValues)
                console.log('insertedComment',insertedComment)

                
                 

                response.status(200).json({
                    msg: 'Comment is Created Successfully',
                    post: await creatPostObject(postId)
                });
            }

          
            
        }

         
           
        
    } catch (error) {

        response.status(500).json({
            errors: [
                {
                    msg: error
                }
            ]
        });
    }
});

/*
    @sno : 8
    @usage : DELETE Comment of a post
    @url : http://127.0.0.1:9999/api/posts/comment/:postId/:commentId
    @fields : no-fields
    @method : DELETE
    @access : PRIVATE
 */
postRouter.delete('/comment/:postId/:commentId', tokenVerifier, async (request: Request, response: Response) => {
    try {
        let {postId, commentId} = request.params;
        let mongoPostID = new mongoose.Types.ObjectId(postId);
        //let mongoCommentID = new mongoose.Types.ObjectId(commentId);
        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];


        const connection = await DBUtil.connectToDB()
        const selectCommentQuery = `SELECT  *  FROM post_comment where post_id = ${postId } AND comment_id =${commentId} `;
        const commentData: any = await queryDatabase(connection, selectCommentQuery, []);
        if (commentData && Array.isArray(commentData) && commentData.length == 0) {
            return response.status(400).json({
                errors: [
                    {
                        msg: 'Comment not exists'
                    }
                ]
            });
        } else {
            if (commentData && Array.isArray(commentData) && commentData.length > 0) {
                const commentRecord = commentData[0]

                if (commentRecord.user_id.toString() !== requestedUser.id) {
                    return response.status(401).json({
                        errors: [
                            {
                                msg: 'User is not authorized'
                            }
                        ]
                    });
                }
                const deleteQuery = `DELETE FROM post_comment where comment_id  = ${commentId }    `;
                const deletedData: any = await queryDatabase(connection, deleteQuery, []);
                 
 
                response.status(200).json({
                    msg: 'Comment is Deleted',
                    post: await creatPostObject(postId)
                });

            }

        }


        
    } catch (error) {
        response.status(500).json({
            errors: [
                {
                    msg: error
                }
            ]
        });
    }
});


async function  creatPostObject(postId: string) {

    const connection = await DBUtil.connectToDB()
    const postSelectQuery = `SELECT  *  FROM posts where post_id = ${postId } `;
    const postData: any = await queryDatabase(connection, postSelectQuery, []);

    if (postData && Array.isArray(postData) && postData.length > 0) {
        var postRecord = postData[0]
        var post: IPost = {
            user_id: postRecord.user,
            text: postRecord.text,
            image: postRecord.image,
            name: postRecord.name,
            avatar: postRecord.avatar,
            likes: [],
            comments: []
        }
          
    
         console.log('postRecord:123 ',postRecord)
        const postLikeQuery = `SELECT  *  FROM post_like where post_id = ${postId }`
        const postLikeData: any = await queryDatabase(connection, postLikeQuery, []);
        
        
    
        const postCommentQuery = `SELECT  *  FROM post_comment where post_id = ${postId }`
        const postCommentData: any = await queryDatabase(connection, postCommentQuery, []);
          
    
        post = {
            user_id: postRecord.user_id,
            post_id: postId,
            text: postRecord.text,
            image: postRecord.image,
            name: postRecord.name,
            avatar: postRecord.avatar,
            likes: postLikeData,
            comments: postCommentData
        }
        return post
    }
     return {}
  

}
function queryDatabase(connection:any, sql:any, values:any) {
    return new Promise((resolve, reject) => {
      connection.query(sql, values, (error:any, results:any) => {
        if (error) {
            console.log('error: ',error)
          reject(error);
        } else {
            console.log("Number of records inserted: " +  results.toString());
            console.log("Number of records inserted: " + results.insertId);
          resolve(results);
        }
      });
    });
  }

  
  
  postRouter.post('/upload', upload.single('image'), (req: Request, res: Response) => {
    // The 'image' parameter is the name of the input field in your form
    // You can access the uploaded file information using req.file
    if (!req.file) {
      return res.status(400).send('No file uploaded.');
    }
  
    // You can process the uploaded file here and store it in your database or file system
  
    res.status(200).send('File uploaded successfully.');
  });

export default postRouter;