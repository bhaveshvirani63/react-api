import {Request, Response, Router} from 'express';
import {body, validationResult} from "express-validator";
import tokenVerifier from "../middleware/TokenVerifier";
import {IEducation, IExperience, IProfile} from "../models/IProfile";
import constants from "../constants.json";
import ProfileTable from "../database/profileSchema";
import mongoose, {Schema, Mongoose, ObjectId} from 'mongoose';
import UserTable from "../database/userSchema";
import { DBUtil } from '../util/DBUtil';
const mysql = require('mysql');

const profileRouter: Router = Router();

/*
    @sno : 1
    @usage : Create a Profile
    @url : http://127.0.0.1:9999/api/profiles/
    @fields : company , website , location , designation , skills , bio ,
    githubUsername, youtube , facebook , twitter , linkedin , instagram
    @method : POST
    @access : PRIVATE
 */
profileRouter.post('/', [
    body('company').not().isEmpty().withMessage('Company is Required'),
    body('website').not().isEmpty().withMessage('website is Required'),
    body('location').not().isEmpty().withMessage('location is Required'),
    body('designation').not().isEmpty().withMessage('designation is Required'),
    body('skills').not().isEmpty().withMessage('skills is Required'),
    body('bio').not().isEmpty().withMessage('bio is Required'),
    body('githubUsername').not().isEmpty().withMessage('githubUsername is Required')
]
    , tokenVerifier, async (request: Request, response: Response) => {

    // validate the form from client
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({errors: errors.array()});
    }

    try {
        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];
        let {
            company, website, location, designation, skills, bio,
            githubUsername
        } = request.body;

        // check if the profile exists with githubUsername

        const connection = await DBUtil.connectToDB()

        const profileQuery = 'SELECT  *  FROM profiles WHERE githubUsername = ?';
        console.log('userQuery:', profileQuery);
        console.log('githubUsername:', githubUsername);
        const resultdata = await queryDatabase(connection, profileQuery, [githubUsername]);
        console.log('resultdata:', resultdata);

        if (resultdata && Array.isArray(resultdata) && resultdata.length > 0) {
            return response.status(401).json({
                errors: [
                    {
                        msg: "Profile is already exists"
                    }
                ]
            })
        }
 
        let skillsArray: [string] = skills
        .split(',')
        // .map((skill: string) => skill.trim());
console.log('Skill array: ', skillsArray)
        let profileObj: IProfile = {
            user: requestedUser.id,
            company: company,
            website: website,
            location: location,
            designation: designation,
            bio: bio,
            githubUsername: githubUsername,
        };

        const query = `
              INSERT INTO profiles (user_id, company, website, location, designation,bio,githubUsername)
              VALUES (?, ?, ?, ?, ?,?,?)
            `;
        const values = [profileObj.user, profileObj.company, profileObj.website, profileObj.location, profileObj.designation, profileObj.bio, profileObj.githubUsername];

        const insertedProfile: any = await queryDatabase(connection, query, values)

        if (insertedProfile && insertedProfile.insertId) {
            const newUserId = insertedProfile.insertId;
            console.log('New user ID:', newUserId);

          
            console.log('===========================')
            for (let index = 0; index < skillsArray.length; index++) {
                console.log('New user ID:', skillsArray[index]);
                const query = `
                INSERT INTO skills (profile_id, skill_name)
                VALUES (?, ?)
              `;
                const values = [newUserId, skillsArray[index]];
    
                const insertedProfile: any = await queryDatabase(connection, query, values)
            }
          }
         
         
        return response.status(200).json({
            msg: 'Profile creation is success!',
            profile:profileObj
        });
    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
    @sno : 2
    @usage : Get my Profile
    @url : http://127.0.0.1:9999/api/profiles/me
    @fields : no-fields
    @method : GET
    @access : PRIVATE
 */
profileRouter.get('/me', tokenVerifier, async (request: Request, response: Response) => {

    try {
        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];
        let mongoUserId = new mongoose.Types.ObjectId(requestedUser.id);

        const connection = await DBUtil.connectToDB()
       
        const query = `
        SELECT profiles.profile_id, users.name, users.avatar, profiles.* FROM users LEFT JOIN profiles ON users.user_id = profiles.user_id where profiles.user_id =${requestedUser.id}; 
      `;
 
      console.log(query)
        const profileData: any = await queryDatabase(connection, query, [])
        var skills :string[] = []

        var educations :IEducation[] = []
        var experiences :IExperience[] = []
        var profile :IProfile = {}

        if (profileData && Array.isArray(profileData) && profileData.length > 0) {
            const profileRecord = profileData[0];
            console.log(profileRecord.profile_id)
            profile.profile_id = profileRecord.profile_id,

            profile.user = profileRecord.user_id,
            profile.company = profileRecord.company,
            profile.website = profileRecord.website,
            profile.location = profileRecord.location,
            profile.designation = profileRecord.designation,
            profile.bio = profileRecord.bio,
            profile.githubUsername = profileRecord.githubUsername
            profile.skills = []
            profile.education = []
            profile.experience = []

            const skillQuery = `
            SELECT skill_name FROM skills where profile_id =${profileRecord.profile_id};  
          `;
     
          console.log(skillQuery)
            const skillData: any = await queryDatabase(connection, skillQuery, [])
            if (skillData && Array.isArray(skillData) && skillData.length > 0) {
                skillData.forEach((item)=>{
                    skills.push(item.skill_name)
                })
                 profile.skills = skills
            }

            const educationQuery = `
            SELECT * FROM education where profile_id =${profileRecord.profile_id};  
          `;
     
          console.log(educationQuery)
            const educationData: any = await queryDatabase(connection, educationQuery, [])
            if (educationData && Array.isArray(educationData) && educationData.length > 0) {
                educationData.forEach((item)=>{
                    educations.push(item)
                })
                 profile.education = educations
            }

            const experienceQuery = `
            SELECT * FROM experience where profile_id =${profileRecord.profile_id};  
          `;
     
          console.log(experienceQuery)
            const experienceData: any = await queryDatabase(connection, experienceQuery, [])
            if (experienceData && Array.isArray(experienceData) && experienceData.length > 0) {
                experienceData.forEach((item)=>{
                    experiences.push(item)
                })
                 profile.experience = experiences
            }
        }
        return response.status(200).json({
            profile: profile
        })

    
        
    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error',
                    error: error
                }
            ]
        });
    }
});

/*
    @sno : 3
    @usage : Update a Profile
    @url : http://127.0.0.1:9999/api/profiles/:profileId
    @fields : company , website , location , designation , skills , bio ,
    githubUsername, youtube , facebook , twitter , linkedin , instagram
    @method : PUT
    @access : PRIVATE
 */

profileRouter.put('/:profileId', [
    body('company').not().isEmpty().withMessage('Company is Required'),
    body('website').not().isEmpty().withMessage('website is Required'),
    body('location').not().isEmpty().withMessage('location is Required'),
    body('designation').not().isEmpty().withMessage('designation is Required'),
    body('skills').not().isEmpty().withMessage('skills is Required'),
    body('bio').not().isEmpty().withMessage('bio is Required'),
    body('githubUsername').not().isEmpty().withMessage('githubUsername is Required'),
    
], tokenVerifier, async (request: Request, response: Response) => {
    // validate the form from client
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({errors: errors.array()});
    }
    console.log('userQuery:');
    try {
        let {profileId} = request.params;
        console.log('userQuery:1');
        // const mongoProfileId = new mongoose.Types.ObjectId(profileId);
        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];
        let {
            company, website, location, designation, skills, bio,
            githubUsername
        } = request.body;
        console.log('userQuery:');

        const connection = await DBUtil.connectToDB()

        // check if the profile exists with githubUsername

        const profileQuery = 'SELECT  *  FROM profiles WHERE  profile_id  = ?';
        console.log('userQuery:', profileQuery);
        console.log('githubUsername:', githubUsername);
        const resultdata = await queryDatabase(connection, profileQuery, [profileId]);
        console.log('resultdata:', resultdata);

        if (resultdata && Array.isArray(resultdata) && resultdata.length == 0) {
            return response.status(401).json({
                errors: [
                    {
                        msg: "Profile is not exists"
                    }
                ]
            })
        }
        

        let skillsArray: string[] = skills.toString().split(',').map((skill: string) => skill.trim());

        let profileObj: IProfile = {
            user: requestedUser.id,
            company: company,
            website: website,
            location: location,
            designation: designation,
            skills: skillsArray,
            bio: bio,
            githubUsername: githubUsername
            
        };

       

        const query = `
        UPDATE profiles SET 
        company = '${profileObj.company}',
        website ='${profileObj.website}', 
        location = '${profileObj.location}',  
        designation = '${profileObj.designation}', 
        bio = '${profileObj.bio}' 
        where profile_id  = ${profileId};
        `;

        const insertedExperience: any = await queryDatabase(connection, query, []) 

        const deleteQuery = `DELETE FROM skills WHERE profile_id  = ${profileId}`;
        console.log('deleteQuery:', deleteQuery);
         const deletedData = await queryDatabase(connection, deleteQuery, []);
        console.log('deletedData:', deletedData); 

        for (let index = 0; index < skillsArray.length; index++) {
            console.log('New user ID:', skillsArray[index]);
            const query = `
            INSERT INTO skills (profile_id, skill_name)
            VALUES (?, ?)
          `;
            const values = [profileId, skillsArray[index]];

            const insertedProfile: any = await queryDatabase(connection, query, values)
        }

        return response.status(200).json({
            msg: 'Profile update is success!',
            profile: profileObj
        });
         
    } catch (error) { 
        console.log('userQuery:', error);

        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
    @sno : 4
    @usage : Get Profile of a User
    @url : http://127.0.0.1:9999/api/profiles/users/:userId
    @fields : no-fields
    @method : GET
    @access : PUBLIC
 */
// profileRouter.get('/users/:userId', async (request: Request, response: Response) => {
//     try {
//         let {userId} = request.params;
//         let mongoUserId = new mongoose.Types.ObjectId(userId);
//         let profile: IProfile | null = await ProfileTable.findOne({user: mongoUserId}).populate('user', ['name', 'avatar']);
//         if (!profile) {
//             return response.status(200).json({
//                 errors: [
//                     {
//                         msg: 'No Profile Found'
//                     }
//                 ]
//             });
//         }
//         return response.status(200).json({
//             profile: profile
//         });

//     } catch (error) {
//         return response.status(500).json({
//             errors: [
//                 {
//                     msg: 'Server Error'
//                 }
//             ]
//         });
//     }

// });

/*
    @sno : 5
    @usage : Delete a Profile , User , Post
    @url : http://127.0.0.1:9999/api/profiles/users/:userId
    @fields : no-fields
    @method : DELETE
    @access : PRIVATE
 */
// profileRouter.delete('/users/:userId', tokenVerifier, async (request: Request, response: Response) => {
//     try {
//         let {userId} = request.params;
//         let mongoUserId = new mongoose.Types.ObjectId(userId);
//         // check profile
//         let profile = await ProfileTable.findOne({user: mongoUserId});
//         if (!profile) {
//             return response.status(200).json({
//                 errors: [
//                     {
//                         msg: 'No Profile Found'
//                     }
//                 ]
//             });
//         }
//         await ProfileTable.findByIdAndRemove(new mongoose.Types.ObjectId(profile._id));

//         // delete the user
//         let user = await UserTable.findById(mongoUserId);
//         if (!user) {
//             return response.status(404).json({
//                 errors: [
//                     {
//                         msg: 'No User Found'
//                     }
//                 ]
//             });
//         }
//         await UserTable.findByIdAndRemove(mongoUserId);

//         // TODO delete post

//         return response.status(200).json(
//             {
//                 msg: 'User Account is deleted'
//             }
//         );
//     } catch (error) {
//         return response.status(500).json({
//             errors: [
//                 {
//                     msg: 'Server Error'
//                 }
//             ]
//         });
//     }
// });

/*
    @sno : 6
    @usage : Add an experience of a Profile
    @url : http://127.0.0.1:9999/api/profiles/experience
    @fields : title , company , location , from , to , current , description
    @method : POST
    @access : PRIVATE
 */
profileRouter.post('/experience/', [
    body('title').not().isEmpty().withMessage('Title is required'),
    body('company').not().isEmpty().withMessage('company is required'),
    body('location').not().isEmpty().withMessage('location is required'),
    body('from').not().isEmpty().withMessage('from is required'),
    body('description').not().isEmpty().withMessage('description is required'),
], tokenVerifier, async (request: Request, response: Response) => {
    // validate the form from client
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({errors: errors.array()});
    }
    try {

        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];

        const connection = await DBUtil.connectToDB()

        const profileQuery = `SELECT  *  FROM profiles WHERE user_id  = ${requestedUser.id}`;
        console.log('userQuery:', profileQuery);
         const resultdata = await queryDatabase(connection, profileQuery, []);
        console.log('resultdata:', resultdata);

        var profileId = ''

        if (resultdata && Array.isArray(resultdata)) {
            if (resultdata.length == 0) {
                return response.status(200).json({
                    errors: [
                        {
                            msg: 'No Profile Found!'
                        }
                    ]
                })
            } else {
                profileId = resultdata[0].profile_id;
            }
            
        } 

         
        let {title, company, location, from, to, current, description} = request.body;
        let experienceObj: IExperience = {
            title: title,
            company: company,
            location: location,
            from: from,
            to: to ? to : '',
            current: current ? current : false,
            description: description
        }

        const query = `
        INSERT INTO experience (profile_id ,title, company, location, experience.from, experience.to,experience.current,description)
        VALUES (?,?, ?, ?, ?, ?,?,?)
      `;
        const values = [profileId, experienceObj.title, experienceObj.company, experienceObj.location,
    experienceObj.from, experienceObj.to, experienceObj.current, experienceObj.description];

        const insertedExperience: any = await queryDatabase(connection, query, values)

        if (insertedExperience && insertedExperience.insertId) {
            const newUserId = insertedExperience.insertId;
            console.log('New user ID:', newUserId);

                
                return response.status(200).json({
                    msg: 'Experience is Added!'
                });
            } 
    }
    
    catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});


/*
    @sno : 7
    @usage : Delete an experience of a Profile
    @url : http://127.0.0.1:9999/api/profiles/experience/:experienceId
    @fields : no-fields
    @method : DELETE
    @access : PRIVATE
 */
profileRouter.delete('/experience/:experienceId', tokenVerifier, async (request: Request, response: Response) => {
    try {
        let {experienceId} = request.params;
 
        const connection = await DBUtil.connectToDB()

        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];
        
        const profileQuery = `SELECT  *  FROM profiles WHERE user_id  = ${requestedUser.id}`;
        console.log('userQuery:', profileQuery);
         const resultdata = await queryDatabase(connection, profileQuery, []);
        console.log('resultdata:', resultdata);

        var profileId = ''

        if (resultdata && Array.isArray(resultdata)) {
            if (resultdata.length == 0) {
                return response.status(200).json({
                    errors: [
                        {
                            msg: 'No Profile Found!'
                        }
                    ]
                })
            } else {
                profileId = resultdata[0].profile_id;
            }
            
        } 

        
        const deleteQuery = `DELETE FROM experience WHERE experience_id  = ${experienceId}`;
        console.log('deleteQuery:', deleteQuery);
         const deletedData = await queryDatabase(connection, deleteQuery, []);
        console.log('deletedData:', deletedData); 

        return response.status(200).json({
            msg: 'Experience is Removed!'
        });


    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
    @sno : 8
    @usage : Add an education of a Profile
    @url : http://127.0.0.1:9999/api/profiles/education
    @fields : school, degree, fieldOfStudy , from , to , current , description
    @method : POST
    @access : PRIVATE
 */
profileRouter.post('/education', [
    body('school').not().isEmpty().withMessage('School is required'),
    body('degree').not().isEmpty().withMessage('degree is required'),
    body('fieldOfStudy').not().isEmpty().withMessage('fieldOfStudy is required'),
    body('from').not().isEmpty().withMessage('from is required'),
    body('description').not().isEmpty().withMessage('description is required')
], tokenVerifier, async (request: Request, response: Response) => {
    // validate the form from client
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({errors: errors.array()});
    }

    try {
        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];
        const connection = await DBUtil.connectToDB()

        const profileQuery = `SELECT  *  FROM profiles WHERE user_id  = ${requestedUser.id}`;
        console.log('userQuery:', profileQuery);
         const resultdata = await queryDatabase(connection, profileQuery, []);
        console.log('resultdata:', resultdata);

        var profileId = ''

        if (resultdata && Array.isArray(resultdata)) {
            if (resultdata.length == 0) {
                return response.status(200).json({
                    errors: [
                        {
                            msg: 'No Profile Found!'
                        }
                    ]
                })
            } else {
                profileId = resultdata[0].profile_id;
            }
            
        } 

        let {school, degree, fieldOfStudy, from, to, current, description} = request.body;
        let educationObj: IEducation = {
            school: school,
            degree: degree,
            fieldOfStudy: fieldOfStudy,
            from: from,
            to: to ? to : '',
            current: current ? current : false,
            description: description
        }

        const query = `
        INSERT INTO education (profile_id ,school, degree, fieldOfStudy, education.from, education.to,education.current,description)
        VALUES (?,?, ?, ?, ?, ?,?,?)
      `;
        const values = [profileId, educationObj.school, educationObj.degree, educationObj.fieldOfStudy,
            educationObj.from, educationObj.to, educationObj.current, educationObj.description];

        const insertedEducation: any = await queryDatabase(connection, query, values)

        if (insertedEducation && insertedEducation.insertId) {
            const newUserId = insertedEducation.insertId;
            console.log('New user ID:', newUserId);

                
                return response.status(200).json({
                    msg: 'Education is Added!'
                });
            } 
       
    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
    @sno : 9
    @usage : Delete an education of a Profile
    @url : http://127.0.0.1:9999/api/profiles/education/:educationId
    @fields : no-fields
    @method : DELETE
    @access : PRIVATE
 */
profileRouter.delete('/education/:educationId', tokenVerifier, async (request: Request, response: Response) => {
    try {
        let {educationId} = request.params;
        console.log('educationId:', educationId); 

        const connection = await DBUtil.connectToDB()

        let requestedUser: any = request.headers[constants.APP_CONSTANTS.USER];
        
        const profileQuery = `SELECT  *  FROM profiles WHERE user_id  = ${requestedUser.id}`;
        console.log('userQuery:', profileQuery);
         const resultdata = await queryDatabase(connection, profileQuery, []);
        console.log('resultdata:', resultdata);

        var profileId = ''

        if (resultdata && Array.isArray(resultdata)) {
            if (resultdata.length == 0) {
                return response.status(200).json({
                    errors: [
                        {
                            msg: 'No Profile Found!'
                        }
                    ]
                })
            } else {
                profileId = resultdata[0].profile_id;
            }
            
        } 

        
        const deleteQuery = `DELETE FROM education WHERE education_id  = ${educationId}`;
        console.log('deleteQuery:', deleteQuery);
         const deletedData = await queryDatabase(connection, deleteQuery, []);
        console.log('deletedData:', deletedData); 

        return response.status(200).json({
            msg: 'Education is Removed!'
        });

       
    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
    @sno : 10
    @usage : Get all Profile
    @url : http://127.0.0.1:9999/api/profiles/all
    @fields : no-fields
    @method : GET
    @access : PUBLIC
 */
// profileRouter.get('/all', async (request: Request, response: Response) => {
//     try {
//         let profiles = await ProfileTable.find().populate('user', ['name', 'avatar']);
//         return response.status(200).json({
//             profiles: profiles
//         });
//     } catch (error) {
//         return response.status(500).json({
//             errors: [
//                 {
//                     msg: 'Server Error'
//                 }
//             ]
//         });
//     }
// });

/*
    @sno : 11
    @usage : Get Profile of a User with Profile Id
    @url : http://127.0.0.1:9999/api/profiles/:profileId
    @fields : no-fields
    @method : GET
    @access : PUBLIC
 */
// profileRouter.get('/:profileId', async (request: Request, response: Response) => {
//     try {
//         let {profileId} = request.params;
//         let mongoProfileId = new mongoose.Types.ObjectId(profileId);
//         let profile: IProfile | null = await ProfileTable.findById(mongoProfileId).populate('user', ['name', 'avatar']);
//         if (!profile) {
//             return response.status(200).json({
//                 errors: [
//                     {
//                         msg: 'No Profile Found!'
//                     }
//                 ]
//             });
//         }
//         return response.status(200).json({
//             profile: profile
//         });
//     } catch (error) {
//         return response.status(500).json({
//             errors: [
//                 {
//                     msg: 'Server Error'
//                 }
//             ]
//         });
//     }
// });
function queryDatabase(connection:any, sql:any, values:any) {
    return new Promise((resolve, reject) => {
      connection.query(sql, values, (error:any, results:any) => {
        if (error) {
            console.log('error: ',error)
          reject(error);
        } else {
            console.log("Number of records inserted: " +  results.toString());
            console.log("Number of records inserted: " + results.insertId);
          resolve(results);
        }
      });
    });
  }

export default profileRouter;