export interface IUser {
    _id?: string;
    name: string;
    email: string;
    password: string;
    isAdmin: boolean;
    avatar: string;
    createdAt?: string;
    updatedAt?: string;
}