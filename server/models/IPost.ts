import {Schema} from 'mongoose';

export interface ILike {
    like_id: string;
    user_id: string;
    post_id: string;
}

export interface IComment {
    comment_id?: string;
    user_id: string;
    text: string;
    name: string;
    avatar: string;
    date: string;
}

export interface IPost {
    post_id?: string;
    user_id: string;
    text: string;
    image: string;
    name: string;
    avatar: string;
    likes: ILike[],
    comments: IComment[],
    createdAt?: string;
    updatedAt?: string;
}