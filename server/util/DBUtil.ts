import mongoose from 'mongoose';
const mysql = require('mysql');

export class DBUtil {
    

    public static connectToDB(): Promise<any>{

        


        return new Promise((resolve, reject) => {
            try {

                const connection = mysql.createConnection({
                    host: 'localhost', // Your MySQL server's host
                    user: 'root',      // Your MySQL username
                    password: '', // Your MySQL password
                    database: 'mysocialapp', // Your MySQL database name
                  });
                connection.connect((error:any) => {
                    if (error) {
                      console.error('Error connecting to the database:', error);
                      reject(connection)
                      return;
                    }
                    console.log('Connected to the MySQL database');
                    resolve(connection)
                  });
                 
            }
            catch (error:any){
                console.error(error);
            }
        })
    }

    public static connectToDB1(dbUrl:string, dbName:string): Promise<any>{
        return new Promise((resolve, reject) => {
            try {
                mongoose.connect(dbUrl, {
                    dbName : dbName
                }).then((response) => {
                    resolve("Connected to DB is success");
                }).catch((error) => {
                    reject('Unable to Connect to DB!');
                    process.exit(0); // stop the node js server
                });
            }
            catch (error){
                console.error(error);
            }
        })
    }
}