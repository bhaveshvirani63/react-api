import mongoose, {Model, Schema} from 'mongoose';
import {IPost} from "../models/IPost";

const postSchema: Schema = new Schema<IPost>({
    user: {type: Schema.Types.ObjectId, ref: 'users', required: true},
    text: {type: String, required: true},
    image: {type: String, required: true},
    name: {type: String, required: true},
    avatar: {type: String, required: true},
    likes: [
        {
            user: {type: Schema.Types.ObjectId, ref: 'users'}
        }
    ],
    comments: [
        {
            user: {type: Schema.Types.ObjectId, ref: 'users'},
            text: {type: String, required: true},
            name: {type: String, required: true},
            avatar: {type: String, required: true},
            date: {type: String, required: true},
        }
    ]
}, {timestamps: true});
const PostTable: Model<IPost> = mongoose.model<IPost>('posts', postSchema);
export default PostTable;