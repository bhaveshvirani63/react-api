import mongoose, {Schema, Model} from 'mongoose';
import {IProfile} from "../models/IProfile";

const profileSchema: Schema = new Schema<IProfile>({
    user: {type: Schema.Types.ObjectId, ref: 'users', required: true},
    company: {type: String, required: true},
    website: {type: String, required: true},
    location: {type: String, required: true},
    designation: {type: String, required: true},
    skills: {type: [String], required: true},
    bio: {type: String, required: true},
    githubUsername: {type: String, required: true, unique: true},
    experience: [
        {
            title: {type: String},
            company: {type: String},
            location: {type: String},
            from: {type: String},
            to: {type: String},
            current: {type: Boolean},
            description: {type: String},
        }
    ],
    education: [
        {
            school: {type: String},
            degree: {type: String},
            fieldOfStudy: {type: String},
            from: {type: String},
            to: {type: String},
            current: {type: Boolean},
            description: {type: String},
        }
    ],
    social: {
        youtube: {type: String, required: true},
        twitter: {type: String, required: true},
        facebook: {type: String, required: true},
        instagram: {type: String, required: true},
        linkedin: {type: String, required: true},
    }
}, {timestamps: true});

const ProfileTable: Model<IProfile> = mongoose.model<IProfile>('profiles', profileSchema);
export default ProfileTable;