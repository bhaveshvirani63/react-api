import jwt from 'jsonwebtoken';
import {Request, Response, NextFunction} from "express";
import constants from "../constants.json";

const tokenVerifier = async (request: Request, response: Response, next: NextFunction) => {
    try {
        // read the token from request
        let secretKey: string | undefined = process.env.JWT_SECRET_KEY;
        if (secretKey) {
            let token = request.headers[constants.APP_CONSTANTS.REQUEST_TOKEN_KEY];
            if (!token) {
                return response.status(401).json({
                    msg: 'No Token Provided!'
                });
            }
            if (typeof token === "string" && secretKey) {
                let decodeObj: any = await jwt.verify(token, secretKey);
                request.headers[constants.APP_CONSTANTS.USER] = decodeObj.user;
                next(); // passing to actual URL
            } else {
                return response.status(401).json({
                    msg: 'An Invalid Token!'
                });
            }
        }
    } catch (error) {
        return response.status(500).json({
            msg: 'Unauthorized!, its an invalid token'
        });
    }
};
export default tokenVerifier;